package com.prueba;

import java.io.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Browser {
	
	
	public static void main(String[] args) throws IOException {
	
		String basePath = new File("").getAbsolutePath();
		System.out.println(basePath);
		String path = new File("/fichero.txt")  .getAbsolutePath();
		 
	    String path2 = new File("resources/Driver/chromedriver.exe") . getAbsolutePath();
		
		String nombre=  "martin";
		String email=  " info@consultoriaglobal.com.ar ";
		String asuntos=  "Tema de una exposición oral o escrita.";
		String mensajes= "Quedo a la espera de su respuesta y una resolución para mi problema. Esperaré 2 meses antes de buscar asistencia de un tercero.";
 		
		FileWriter fichero = new FileWriter(path);
	
		String projectPath = System.getProperty("user.dir");
		
		System.out.println("projecthPath : "+projectPath);
		
		System.setProperty("webdriver.chrome.driver", path2);
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://www.consultoriaglobal.com.ar/cgweb/");
		
		System.out.println("Ingreso a consultoria global");
		
		try {
			Thread.sleep(3000);
			System.out.println("Esperando");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.get("https://www.consultoriaglobal.com.ar/cgweb/?page_id=370");
	
		System.out.println("Ingreso a contacto de consultoria global");
		
		WebElement name = driver.findElement(By.name("your-name")); 
		
		name.sendKeys(nombre);
		
		fichero.write("\n" + nombre);
		
		 WebElement mail = driver.findElement(By.name("your-email")); 
		 
		 mail.sendKeys(email);
		 
		 //info@consultoriaglobal.com.ar 
		 System.out.println("mail ingresado");
		 
		 fichero.write("\n" + email);
		
		 WebElement asunto = driver.findElement(By.name("your-subject")); 
		 
		 asunto.sendKeys(asuntos);
		 
		 fichero.write("\n" + asuntos);
		 
		 System.out.println("asunto ingresado");
		 
		 WebElement mensaje = driver.findElement(By.name("your-message")); 
		 
		 mensaje.sendKeys(mensajes);
		 
		 fichero.write("\n" + mensajes);
		 
		 System.out.println("mensaje ingresado");
		 
		System.out.println("Formulario Cargado");
		
		try {
		Thread.sleep(9000);
		
		System.out.println("Esperando");
		
		driver.findElement(By.className("wpcf7-submit")).click();
		
		System.out.println("Enviando");
		

		Thread.sleep(7000);
		
		
		if(!driver.findElements(By.className("wpcf7-not-valid-tip")).isEmpty()){
			
			System.out.println("Mail Incorrecto");
			fichero.write("\n Mail Incorrecto");
		}else {
			
			System.out.println("El mail es Correcto");
			fichero.write("\n Mail Correcto");
		}
		
		}  catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		fichero.close();
		driver.close();
		
	}
	
}
